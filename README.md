# Tinder voor foto's

**Features**

- U kunt foto's "liken" of "disliken". Hierna verschijnt er een nieuwe foto.
- U kunt navigeren naar een plek waar alle gelikete foto's staan.
- Als u op de foto klikt, wordt hij vergroot en toont hij een beschrijving.

De Dog Tinder kunt u **[hier](https://zippy-narwhal-467c67.netlify.app/)** vinden.