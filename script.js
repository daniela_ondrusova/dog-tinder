const pic = document.querySelector(".pic");
const heart = document.querySelector(".heart");
const cross = document.querySelector(".cross");
const favs = document.querySelector(".favs");


const img = document.createElement("img");
img.src = "..";
img.alt = "dogs";
img.className = "dog";

pic.append(img);

async function dogs() {
    fetch("https://dog.ceo/api/breeds/image/random")
        .then(res => res.json())
        .then(data => {
            img.src = data.message;
        });
}

dogs();


heart.addEventListener("click", async (e) => {
    e.preventDefault();
    await dogs();
    await close();

    if (!localStorage.getItem("Favs")) {
        localStorage.setItem("Favs", JSON.stringify([]));
    }
    let b = JSON.parse(localStorage.getItem("Favs"));
    b.push(img.src);
    localStorage.setItem("Favs", JSON.stringify(b));
});

cross.addEventListener("click", async () => {
    await dogs();
    await close();
});

const after = document.querySelector(".after");
const name1 = document.querySelector(".name");
const description = document.querySelector(".desc");
const container = document.querySelector(".container");
const header = document.querySelector(".header");
const close1 = document.querySelector(".close");

close1.style.display = "none";

img.addEventListener("click", () => {
    img.style.transform = "scale(1.3)";
    img.style.transition = "transform 0.25s ease";
    pic.style.height = "610px";
    header.style.height = "190px";

    after.classList.add("after-click");

    heart.style.marginRight = "10px";
    name1.style.marginBottom = "10px";

    const url = new URL(img.src);
    const url1 = url.pathname.split('/')[2];
    name1.textContent = url1.charAt(0).toUpperCase() + url1.slice(1);

    description.textContent = "Lorem ipsum dolor sit amet consectetur, adipisicing elit. Sint, aspernatur!";

    close1.style.display = "block";
});


async function close() {
    img.style.transform = "scale(1)";
    img.style.transition = "transform 0.25s ease";
    pic.style.height = "504px";
    header.style.height = "88px";

    after.classList.add("after-click");
    after.classList.remove("after-click");

    heart.style.marginRight = "0";
    name1.style.marginBottom = "0";

    name1.textContent = "";
    description.textContent = "";
    close1.style.display = "none";
}

close1.addEventListener("click", close);