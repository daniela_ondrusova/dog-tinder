const storage = JSON.parse(localStorage.getItem("Favs")) || [];
const template = document.querySelector("template");
const tbody = document.querySelector("tbody");


function showTable() {
    for (let i = 0; i < storage.length; i++) {
        const clone = template.content.cloneNode(true);
        tbody.appendChild(clone);


        const image = document.querySelectorAll(".image");
        const namePr = document.querySelectorAll(".name");
        const description = document.querySelectorAll(".desc");

        image[i].src = storage[i];
        const url = new URL(storage[i]);
        const url1 = url.pathname.split('/')[2];
        namePr[i].textContent = url1.charAt(0).toUpperCase() + url1.slice(1);
    }
}
showTable();

for (let i = 0; i < storage.length; i++) {
    const del = document.querySelectorAll(".delete");
    const tr = document.querySelectorAll("tr");
    let storage1 = JSON.parse(localStorage.getItem("Favs")) || [];

    del[i].addEventListener("click", () => {
        tr[i].remove();

        storage1 = storage1.filter(item => item !== storage1[i]);
        localStorage.setItem("Favs", JSON.stringify(storage1));
        window.location.reload();
    });
}